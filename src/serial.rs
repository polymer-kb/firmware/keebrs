//! Utilities for sending keeb info over serial

#[cfg(test)]
mod test;

use alloc::string::String;

use serde::{
    Deserialize,
    Serialize,
};

use crate::key::PhysKey;

/// A message to be sent over a serial port
#[derive(Deserialize, Serialize, Debug, Clone, Eq, PartialEq)]
pub enum Msg {
    /// Initiaization and claiming of the usb connection
    Init,
    /// Module type announcement from a secondary module
    ModuleType(String),
    /// Physical key press/release event from a secondary module to the primary
    /// controller.
    KeyEvent(PhysKey),
    /// Message indicating that the backlight effect should change
    ChangeBacklight,
}
