//! Traits representing stateful keyboard scanning

use core::{
    future::Future,
    pin::Pin,
    task::{
        Context,
        Poll,
    },
};

mod scanner;
pub use scanner::{
    StatefulScanner,
    VecScanner,
};

use crate::key::*;

/// Trait for scanning with state
///
/// Rather than row/column level manipulation, this will return only the
/// press/release events.
pub trait ScanStateful {
    /// Poll the keyboard for changes
    fn poll_change(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Option<PhysKey>>;
}

/// Extension trait for [ScanStateful]
pub trait ScanStatefulExt: ScanStateful + Unpin {
    /// Try to get a key change as a [Future]
    fn change(&mut self) -> ScanStatefulFut<Self> {
        ScanStatefulFut { scanner: self }
    }
}

impl<S> ScanStatefulExt for S where S: ScanStateful + Unpin {}

/// The [Future] returned by [ScanStatefulExt::change]
pub struct ScanStatefulFut<'s, S: ?Sized> {
    scanner: &'s mut S,
}

impl<'s, S> Future for ScanStatefulFut<'s, S>
where
    S: ScanStateful + Unpin,
{
    type Output = Option<PhysKey>;
    fn poll(self: Pin<&mut Self>, cx: &mut Context<'_>) -> Poll<Self::Output> {
        let this = self.get_mut();
        Pin::new(&mut *this.scanner).poll_change(cx)
    }
}
