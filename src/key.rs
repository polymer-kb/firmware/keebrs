//! Key representations
//!

use serde::{
    Deserialize,
    Serialize,
};

use newtype::NewType;

use crate::keycode::KeyCode;

/// The matrix position of a key
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Copy, Ord, PartialOrd)]
pub struct KeyPos {
    /// The "pulled" line of the key
    pub x: u8,
    /// The "read" line of the key
    pub y: u8,
}

/// The state of a key
///
/// `true` == "pressed"
///
/// `false` == "not pressed"
#[derive(Serialize, Deserialize, NewType, Debug, PartialEq, Eq, Clone, Copy)]
pub struct KeyState(pub bool);

/// A physical key's position and state
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Copy)]
pub struct PhysKey {
    /// The position of the key
    pub pos: KeyPos,
    /// The state of the key
    pub state: KeyState,
}

/// A physical key with board information
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Copy)]
pub struct ModuleKey {
    /// The module the key belongs to
    pub module: u8,
    /// They key's physical position and state
    pub key: PhysKey,
}

/// A resolved key with a KeyCode
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq, Clone, Copy)]
pub struct LogiKey {
    /// The actual keycode
    pub code: KeyCode,

    /// The state of the key
    pub state: KeyState,
}
