{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    rust-overlay.url = "github:nix-community/fenix";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils, fenix, ... }@inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [
          fenix.overlays.default
          (final: prev: {
            fenixToolchain = with prev.fenix; combine [
              (complete.withComponents [
                "llvm-tools-preview"
                "cargo"
                "rust-src"
                "rustc"
                "rustfmt"
                "clippy"
              ])
            ];
            fenixPlatform = prev.makeRustPlatform {
              rustc = final.fenixToolchain;
              cargo = final.fenixToolchain;
            };
          })
        ];
        pkgs = import nixpkgs {
          inherit system overlays;
        };
      in
      with pkgs;
      {
        devShells.default = mkShell {
          buildInputs = [
            fenixToolchain
            rust-analyzer-nightly
          ];
        };
      }
    );
}
